/*jshint browser:true */
/*global $ */(function()
{
 "use strict";
 /*
   hook up event handlers 
 */
 function register_event_handlers()
 {
        /* button  #btn_efetuar_login */
    Verifica_historico_senha(); //Função que verifica se o usuário escolheu salvar usuário e senha. Ela seta o checkbox e coloca o
    
        /* button  #btn_recuperar_senha */
    $(document).on("click", "#btn_recuperar_senha", function(evt)
    {
         /*global activate_subpage */
         activate_subpage("#tela_recuperar_senha"); 
    });
    
        /* button  #btn_voltar */
    $(document).on("click", "#btn_voltar", function(evt)
    {
        
        
    });

        /* button  #btn_efetuar_login */
    $(document).on("click", "#btn_efetuar_login", function(evt)
    {
        Faz_login($("#ipt_email").val(),$("#ipt_senha").val());
    });
    
    
        /* graphic button  .uib_w_37 */
    $(document).on("click", ".uib_w_37", function(evt)
    {
         /*global activate_subpage */
         activate_subpage("#tela_efetuar_cadastro_areadisciplina"); 
    });
    
        /* button  #btn_efetuar_novocadastro */
    $(document).on("click", "#btn_efetuar_novocadastro", function(evt)
    {
         /*global activate_subpage */
         activate_subpage("#tela_efetuar_cadastro"); 
    });
    
        /* button  #btn_efetuar_cadastro */
    $(document).on("click", "#btn_efetuar_cadastro", function(evt)
    {
         Efetuar_cadastro();
    });
    
	
	  $(document).on("onload", "#tela_efetuar_cadastro", function(evt)
	{
		alert ("oi");
	});
	
    }
 document.addEventListener("app.Ready", register_event_handlers, false);
})();
