/*###############################################################################################*/
//FUNÇÕES AUXILIARES

function Faz_login(email,senha) 
{
    
    if (Checa_conexao())
        {
            /*Login pelo banco do back end*/
			if (!(localStorage.getItem('login') == "1")) //Status no localstorage ('login',"1") não está marcado. O usuário não quer salvar usario e senha.
			{
				 if (document.getElementById("chk_salvar_senha").checked)  //Se ele marcou agora para salvar
					{
						localStorage.setItem('login',"1");	 //Salva no localstorage os três itens: Login, Email e Senha
						localStorage.setItem('email',email);
						localStorage.setItem('senha',senha);
					}
			}
			else
			{
				if (!(document.getElementById("chk_salvar_senha").checked))//Caso o usuário estivesse salvando a senha mas desistiu
				{
						localStorage.clear('login');	 
						localStorage.clear('email');
						localStorage.clear('senha');
				}
			}
			
			Ajax_login(email,senha,"1"); //Função AJAX para efetuar login com parâmetro "1". ParÂmetro que será lido pelo api.phpe direcionado para o crud.php
        
        }
    else
        {
            /*Login pelo banco front end. Ainda não implementado*/
		
        }
    
}


function Efetuar_cadastro()
{
	var tipo_cadastro;
			
			
            if ($("#rdo_professor").is(":checked"))
                {
                    tipo_cadastro = "1"; //Representa que é um professor
                }
            else
                {
                    if ($("#ipt_estudante").is(":checked"))
                     {
                         tipo_cadastro = "2";
                     }
                    else
                    {
                     //Aviso que o usuário esqueceu de selecionar se é professor ou aluno
                    }
                }
         
            var valores = [
          
                email,
                senha,
                $("#ipt_nome").val(),
                tipo_cadastro,
                "1",//Seta para 1 campo de usuário cadastrado
                $("#cmb_escolas").val()
             
                        ];

            Ajax_cadastro(valores,"3"); 
       
}

/*###############################################################################################*/
//FUNÇÕES AJAX DE SOLICITAÇÃO AO BANCO

function Ajax_login (email,senha,tipo_requisicao)
{

        $.ajax({   
         //url: "http://localhost/api.php",   
         url: "http://apprendendo.esy.es/api.php",   
         type : 'POST',   
         dataType: "json",
         data : {   
           email: email,   
           senha: senha,   
           requisicao: tipo_requisicao,
         },   
    
         success : function (retorno_login)
          {
                
             if(retorno_login[2] == "1")
                 {     
                       localStorage.setItem('tipo',retorno_login[1]);  
                     
                       activate_subpage("#tela_inicio");  //Login efetuado com sucesso
                       $("#login_mensagem").hide();
                       $("#botao_novocadastro").hide();
                 }
              else
                  {
                      if (retorno_login[2] == "2") //Prov�vel erro de senha
                          {
                            
                              $("#ipt_senha").css("border", "1px solid #FF6347");
                              $("#ipt_senha").css("background", "1px solid #FFB6C1");
							  document.getElementById("login_mensagem_atencao").style.display="block";
                              $("#login_mensagem_atencao").style.display = "block";
							  localStorage.clear('email');
							  localStorage.clear('senha');
                              
                          }
                      else //Prov�vel primeiro acesso
                          {
                              
                              
							  document.getElementById("login_mensagem_aviso").style.display= "block";
							  $("#btn_efetuar_novocadastro").show();
							  $("#btn_efetuar_login").hide();
							  $("#btn_recuperar_senha").hide();
							  document.getElementById("#chk_salvar_senha").style.display= "none";  //Login efetuado com sucesso
                                
                          }
                  } 
            
         },
        error: function ()
                {
                    document.getElementById("login_mensagem_erro").style.display="block";
                }
       
       });     

}



function Ajax_cadastro(valores, tipo_requisicao)
{
    $.ajax({   
         //url: "http://localhost/api.php",   
         url: "http://apprendendo.esy.es/api.php",   
         type : 'POST',  
        dataType: "json",
         data : {   
           email: valores[0],
           senha: valores[1],
           nome: valores[2],
           tipo: valores[3],
           acesso: valores[4],
           escola: valores[5],   
           requisicao: tipo_requisicao,
             
         }, 
         success : function (retorno_cadastro)
          {
                    alert ("Cadastro efetuado com sucesso!");//Mudar essa mensagem para algo no padrão de interface
                    $(location).attr("href", "tela_inicio.html");
                    
          },
        error: function ()
                {
                    alert ("Erro na função");
                }
       
       });   
}



function Ajax_escolas(tipo_requisicao)
{

    $.ajax({   
        //url: "http://localhost/api.php",    
        url: "http://apprendendo.esy.es/api.php",   
         type : 'GET',   
         data : {   
             requisicao: tipo_requisicao,
         },  
         context: jQuery("#cmd_escolas"),
         success : function (retorno_escolas)
        
          {
           
              this.append(retorno_escolas);

          },
        error: function ()
                {
                    alert ("Erro na função");
                }
       
       });   
}



